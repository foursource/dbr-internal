package dbr

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"strconv"
	"strings"

	"github.com/google/uuid"
)

// UpdateStmt builds `UPDATE ...`.
type UpdateStmt struct {
	runner
	EventReceiver
	SyncEventReceiver
	Dialect

	raw

	Table        string
	Value        map[string]interface{}
	WhereCond    []Builder
	ReturnColumn []string
	LimitCount   int64
	Pid          uuid.UUID
	Payload      []map[string]interface{}
	Ctx          context.Context

	comments Comments
}

type UpdateBuilder = UpdateStmt

func (b *UpdateStmt) GetPid() uuid.UUID {
	return b.Pid
}

// Get Table Name
func (b *UpdateStmt) GetTable() string {
	arrayStrings := strings.Split(b.Table, ".")
	return arrayStrings[1]
}

// Get Schema Name
func (b *UpdateStmt) GetSchema() string {
	arrayStrings := strings.Split(b.Table, ".")
	return arrayStrings[0]
}

func (b *UpdateStmt) GetWhereColumns(payload map[string]interface{}) map[string]interface{} {

	whereClause := map[string]interface{}{}

	for col, val := range payload {
		whereClause[col] = val
	}

	for col := range b.Value {
		delete(whereClause, col)
	}

	return whereClause
}

func (b *UpdateStmt) EventQuery() {
	for _, p := range b.Payload {
		b.SyncEventReceiver.EventQuery(
			b.Pid,
			Event{
				Data:      p,
				Query:     b.GetWhereColumns(p),
				Table:     b.GetTable(),
				Schema:    b.GetSchema(),
				Operation: "UPDATE",
			},
		)
	}

}

func (b *UpdateStmt) EventQueryError(err error) {
	if b.Table == "" {
		return
	}
	b.SyncEventReceiver.EventQueryError(
		b.Pid,
		Event{
			Table:     b.GetTable(),
			Schema:    b.GetSchema(),
			Operation: "UPDATE",
		},
		err,
	)

}

func (b *UpdateStmt) Build(d Dialect, buf Buffer) error {
	if b.raw.Query != "" {
		return b.raw.Build(d, buf)
	}

	if b.Table == "" {
		return ErrTableNotSpecified
	}

	if len(b.Value) == 0 {
		return ErrColumnNotSpecified
	}

	err := b.comments.Build(d, buf)
	if err != nil {
		return err
	}

	buf.WriteString("UPDATE ")
	buf.WriteString(d.QuoteIdent(b.Table))
	buf.WriteString(" SET ")

	i := 0
	for col, v := range b.Value {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(d.QuoteIdent(col))
		buf.WriteString(" = ")
		buf.WriteString(placeholder)

		buf.WriteValue(v)
		i++
	}

	if len(b.WhereCond) > 0 {
		buf.WriteString(" WHERE ")
		err := And(b.WhereCond...).Build(d, buf)
		if err != nil {
			return err
		}
	}

	if b.LimitCount >= 0 {
		buf.WriteString(" LIMIT ")
		buf.WriteString(strconv.FormatInt(b.LimitCount, 10))
	}

	buf.WriteString(" RETURNING *")

	return nil
}

// Update creates an UpdateStmt.
func Update(table string) *UpdateStmt {
	return &UpdateStmt{
		Table:      table,
		Value:      make(map[string]interface{}),
		LimitCount: -1,
	}
}

// Update creates an UpdateStmt.
func (sess *Session) Update(table string) *UpdateStmt {
	b := Update(table)
	b.runner = sess.Master
	b.EventReceiver = sess.EventReceiver
	b.SyncEventReceiver = sess.SyncEventReceiver
	b.Dialect = sess.Master.Dialect
	b.Payload = []map[string]interface{}{}
	return b
}

// Update creates an UpdateStmt.
func (tx *Tx) Update(table string) *UpdateStmt {
	b := Update(table)
	b.runner = tx
	b.EventReceiver = tx.EventReceiver
	b.SyncEventReceiver = tx.SyncEventReceiver
	b.Dialect = tx.Dialect
	b.Payload = []map[string]interface{}{}
	b.Pid = tx.Pid
	return b
}

// UpdateBySql creates an UpdateStmt with raw query.
func UpdateBySql(query string, value ...interface{}) *UpdateStmt {
	return &UpdateStmt{
		raw: raw{
			Query: query,
			Value: value,
		},
		Value:      make(map[string]interface{}),
		LimitCount: -1,
	}
}

// UpdateBySql creates an UpdateStmt with raw query.
func (sess *Session) UpdateBySql(query string, value ...interface{}) *UpdateStmt {
	b := UpdateBySql(query, value...)
	b.runner = sess.Master
	b.EventReceiver = sess.EventReceiver
	b.SyncEventReceiver = sess.SyncEventReceiver
	b.Dialect = sess.Master.Dialect
	b.Payload = []map[string]interface{}{}
	return b
}

// UpdateBySql creates an UpdateStmt with raw query.
func (tx *Tx) UpdateBySql(query string, value ...interface{}) *UpdateStmt {
	b := UpdateBySql(query, value...)
	b.runner = tx
	b.EventReceiver = tx.EventReceiver
	b.SyncEventReceiver = tx.SyncEventReceiver
	b.Dialect = tx.Dialect
	b.Pid = tx.Pid
	b.Payload = []map[string]interface{}{}
	return b
}

// Where adds a where condition.
// query can be Builder or string. value is used only if query type is string.
func (b *UpdateStmt) Where(query interface{}, value ...interface{}) *UpdateStmt {
	switch query := query.(type) {
	case string:
		b.WhereCond = append(b.WhereCond, Expr(query, value...))
	case Builder:
		b.WhereCond = append(b.WhereCond, query)
	}
	return b
}

// Set updates column with value.
func (b *UpdateStmt) Set(column string, value interface{}) *UpdateStmt {
	b.Value[column] = value
	return b
}

// SetMap specifies a map of (column, value) to update in bulk.
func (b *UpdateStmt) SetMap(m map[string]interface{}) *UpdateStmt {
	for col, val := range m {
		b.Set(col, val)
	}
	return b
}

// IncrBy increases column by value
func (b *UpdateStmt) IncrBy(column string, value interface{}) *UpdateStmt {
	b.Value[column] = Expr("? + ?", I(column), value)
	return b
}

// DecrBy decreases column by value
func (b *UpdateStmt) DecrBy(column string, value interface{}) *UpdateStmt {
	b.Value[column] = Expr("? - ?", I(column), value)
	return b
}

func (b *UpdateStmt) Limit(n uint64) *UpdateStmt {
	b.LimitCount = int64(n)
	return b
}

func (b *UpdateStmt) Comment(comment string) *UpdateStmt {
	b.comments = b.comments.Append(comment)
	return b
}

func (b *UpdateStmt) AddContext(ctx context.Context) *UpdateStmt {
	b.Ctx = ctx
	return b
}

func (b *UpdateStmt) Exec() (sql.Result, error) {
	count, err := b.LoadContext(b.Ctx, &b.Payload)
	return driver.RowsAffected(count), err
}

func (b *UpdateStmt) ExecContext(ctx context.Context) (sql.Result, error) {
	count, err := b.LoadContext(ctx, &b.Payload)
	return driver.RowsAffected(count), err
}

func (b *UpdateStmt) LoadContext(ctx context.Context, value *[]map[string]interface{}) (int, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	count, err := queryForMap(ctx, b.runner, b.EventReceiver, b, b.Dialect, value)
	if err != nil {
		b.EventQueryError(err)

	} else {
		b.EventQuery()
	}
	return count, err
}

// Returning specifies the returning columns for postgres.
func (b *UpdateStmt) Returning(column ...string) *UpdateStmt {
	b.ReturnColumn = column
	return b
}

func (b *UpdateStmt) Load(value *int) error {
	_, err := b.LoadContext(b.Ctx, &b.Payload)
	if err != nil {
		return err
	}
	if len(b.ReturnColumn) > 0 {
		*value = int(b.Payload[0][b.ReturnColumn[0]].(int64))
	}
	return nil
}
