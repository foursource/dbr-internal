package dbr

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"github.com/google/uuid"
	"strconv"
	"strings"
)

// DeleteStmt builds `DELETE ...`.
type DeleteStmt struct {
	runner
	EventReceiver
	SyncEventReceiver
	Dialect

	raw

	Table      string
	WhereCond  []Builder
	LimitCount int64
	Pid        uuid.UUID
	Payload    []map[string]interface{}
	Ctx        context.Context

	comments Comments
}

type DeleteBuilder = DeleteStmt

func (b *DeleteStmt) GetPid() uuid.UUID {
	return b.Pid
}

// Get Schema Name
func (b *DeleteStmt) GetTable() string {
	arrayStrings := strings.Split(b.Table, ".")
	return arrayStrings[1]
}

// Get Schema Name
func (b *DeleteStmt) GetSchema() string {
	arrayStrings := strings.Split(b.Table, ".")
	return arrayStrings[0]
}

func (b *DeleteStmt) EventQuery() {

	for _, p := range b.Payload {
		b.SyncEventReceiver.EventQuery(
			b.Pid,
			Event{
				Data:      p,
				Table:     b.GetTable(),
				Schema:    b.GetSchema(),
				Operation: "DELETE",
			},
		)
	}
}

func (b *DeleteStmt) EventQueryError(err error) {
	if b.Table == "" {
		return
	}
	b.SyncEventReceiver.EventQueryError(
		b.Pid,
		Event{
			Table:     b.GetTable(),
			Schema:    b.GetSchema(),
			Operation: "DELETE",
		},
		err,
	)

}

func (b *DeleteStmt) Build(d Dialect, buf Buffer) error {
	if b.raw.Query != "" {
		return b.raw.Build(d, buf)
	}

	if b.Table == "" {
		return ErrTableNotSpecified
	}

	err := b.comments.Build(d, buf)
	if err != nil {
		return err
	}

	buf.WriteString("DELETE FROM ")
	buf.WriteString(d.QuoteIdent(b.Table))

	if len(b.WhereCond) > 0 {
		buf.WriteString(" WHERE ")
		err := And(b.WhereCond...).Build(d, buf)
		if err != nil {
			return err
		}
	}

	if b.LimitCount >= 0 {
		buf.WriteString(" LIMIT ")
		buf.WriteString(strconv.FormatInt(b.LimitCount, 10))
	}

	buf.WriteString(" RETURNING *")

	return nil
}

// DeleteFrom creates a DeleteStmt.
func DeleteFrom(table string) *DeleteStmt {
	return &DeleteStmt{
		Table:      table,
		LimitCount: -1,
	}
}

// DeleteFrom creates a DeleteStmt.
func (sess *Session) DeleteFrom(table string) *DeleteStmt {
	b := DeleteFrom(table)
	b.runner = sess.Master
	b.EventReceiver = sess.EventReceiver
	b.SyncEventReceiver = sess.SyncEventReceiver
	b.Dialect = sess.Master.Dialect
	b.Payload = []map[string]interface{}{}
	return b
}

// DeleteFrom creates a DeleteStmt.
func (tx *Tx) DeleteFrom(table string) *DeleteStmt {
	b := DeleteFrom(table)
	b.runner = tx
	b.EventReceiver = tx.EventReceiver
	b.SyncEventReceiver = tx.SyncEventReceiver
	b.Dialect = tx.Dialect
	b.Payload = []map[string]interface{}{}
	b.Pid = tx.Pid
	return b
}

// DeleteBySql creates a DeleteStmt from raw query.
func DeleteBySql(query string, value ...interface{}) *DeleteStmt {
	return &DeleteStmt{
		raw: raw{
			Query: query,
			Value: value,
		},
		LimitCount: -1,
	}
}

// DeleteBySql creates a DeleteStmt from raw query.
func (sess *Session) DeleteBySql(query string, value ...interface{}) *DeleteStmt {
	b := DeleteBySql(query, value...)
	b.runner = sess.Master
	b.EventReceiver = sess.EventReceiver
	b.SyncEventReceiver = sess.SyncEventReceiver
	b.Dialect = sess.Master.Dialect
	b.Payload = []map[string]interface{}{}
	return b
}

// DeleteBySql creates a DeleteStmt from raw query.
func (tx *Tx) DeleteBySql(query string, value ...interface{}) *DeleteStmt {
	b := DeleteBySql(query, value...)
	b.runner = tx
	b.EventReceiver = tx.EventReceiver
	b.SyncEventReceiver = tx.SyncEventReceiver
	b.Dialect = tx.Dialect
	b.Payload = []map[string]interface{}{}
	b.Pid = tx.Pid
	return b
}

// Where adds a where condition.
// query can be Builder or string. value is used only if query type is string.
func (b *DeleteStmt) Where(query interface{}, value ...interface{}) *DeleteStmt {
	switch query := query.(type) {
	case string:
		b.WhereCond = append(b.WhereCond, Expr(query, value...))
	case Builder:
		b.WhereCond = append(b.WhereCond, query)
	}
	return b
}

func (b *DeleteStmt) Limit(n uint64) *DeleteStmt {
	b.LimitCount = int64(n)
	return b
}

func (b *DeleteStmt) Comment(comment string) *DeleteStmt {
	b.comments = b.comments.Append(comment)
	return b
}

func (b *DeleteStmt) AddContext(ctx context.Context) *DeleteStmt {
	b.Ctx = ctx
	return b
}

func (b *DeleteStmt) Exec() (sql.Result, error) {
	count, err := b.LoadContext(b.Ctx, &b.Payload)
	return driver.RowsAffected(count), err
}

func (b *DeleteStmt) ExecContext(ctx context.Context) (sql.Result, error) {
	count, err := b.LoadContext(ctx, &b.Payload)
	return driver.RowsAffected(count), err
}

func (b *DeleteStmt) LoadContext(ctx context.Context, value *[]map[string]interface{}) (int, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	count, err := queryForMap(ctx, b.runner, b.EventReceiver, b, b.Dialect, value)
	if err != nil {
		b.EventQueryError(err)
	} else {
		b.EventQuery()
	}
	return count, err
}
