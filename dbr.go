// Package dbr provides additions to Go's database/sql for super fast performance and convenience.
package dbr

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"bitbucket.org/foursource/dbr-internal/dialect"
)

type ConnectionConfig struct {
	Driver string
	Dsn    string
}

// Open creates a Connection.
// log can be nil to ignore logging.
func Open(driver, dsn string, log EventReceiver, sync SyncEventReceiver) (*Connection, error) {
	if log == nil {
		log = nullReceiver
	}

	if sync == nil {
		sync = nullSyncReceiver
	}
	// connection
	conn, err := sql.Open(driver, dsn)
	if err != nil {
		return nil, err
	}

	dialect, err := loadDialect(driver)
	if err != nil {
		return nil, err
	}

	db := &DbAccess{DB: conn, Dialect: dialect}

	return &Connection{Slave: db, Master: db, EventReceiver: log, SyncEventReceiver: sync}, nil
}

// Open creates a Connection with multi read-write connections.
// log can be nil to ignore logging.
func OpenMultiConnection(masterConnConfig *ConnectionConfig, slaveConnConfig *ConnectionConfig, log EventReceiver, sync SyncEventReceiver) (*Connection, error) {
	if log == nil {
		log = nullReceiver
	}

	if sync == nil {
		sync = nullSyncReceiver
	}
	if masterConnConfig == nil {
		return nil, fmt.Errorf("sql: unknown master connection configuration (forgotten?)")
	}

	if slaveConnConfig == nil {
		return nil, fmt.Errorf("sql: unknown slave connection configuration (forgotten?)")
	}

	// master connection
	masterConn, err := sql.Open(masterConnConfig.Driver, masterConnConfig.Dsn)
	if err != nil {
		return nil, err
	}

	masterDialect, err := loadDialect(masterConnConfig.Driver)
	if err != nil {
		return nil, err
	}

	// slave connection
	slaveConn, err := sql.Open(slaveConnConfig.Driver, slaveConnConfig.Dsn)
	if err != nil {
		return nil, err
	}

	slaveDialect, err := loadDialect(slaveConnConfig.Driver)
	if err != nil {
		return nil, err
	}

	return &Connection{
		Master:            &DbAccess{DB: masterConn, Dialect: masterDialect},
		Slave:             &DbAccess{DB: slaveConn, Dialect: slaveDialect},
		EventReceiver:     log,
		SyncEventReceiver: sync,
	}, nil
}

func loadDialect(driver string) (d Dialect, err error) {
	switch driver {
	case "mysql":
		d = dialect.MySQL
	case "postgres", "pgx":
		d = dialect.PostgreSQL
	case "sqlite3":
		d = dialect.SQLite3
	default:
		return nil, ErrNotSupported
	}

	return d, nil
}

const (
	placeholder = "?"
)

// Connection wraps sql.DB with an EventReceiver
// to send events, errors, and timings.
type Connection struct {
	Master *DbAccess
	Slave  *DbAccess
	EventReceiver
	SyncEventReceiver
}

type DbAccess struct {
	*sql.DB
	Dialect
	Timeout time.Duration
}

// Session represents a business unit of execution.
//
// All queries in gocraft/dbr are made in the context of a session.
// This is because when instrumenting your app, it's important
// to understand which business action the query took place in.
//
// A custom EventReceiver can be set.
//
// Timeout specifies max duration for an operation like Select.
type Session struct {
	*Connection
	EventReceiver
	SyncEventReceiver
	Timeout time.Duration
}

// GetTimeout returns current timeout enforced in session.
func (dbAccess *DbAccess) GetTimeout() time.Duration {
	return dbAccess.Timeout
}

// NewSession instantiates a Session from Connection.
// If log is nil, Connection EventReceiver is used.
func (conn *Connection) NewSession(log EventReceiver) *Session {
	if log == nil {
		log = conn.EventReceiver // Use parent instrumentation
	}
	return &Session{Connection: conn, EventReceiver: log, SyncEventReceiver: conn.SyncEventReceiver}
}

// Ensure that tx and session are session runner
var (
	_ SessionRunner = (*Tx)(nil)
	_ SessionRunner = (*Session)(nil)
)

// SessionRunner can do anything that a Session can except start a transaction.
// Both Session and Tx implements this interface.
type SessionRunner interface {
	Select(column ...string) *SelectBuilder
	SelectBySql(query string, value ...interface{}) *SelectBuilder

	InsertInto(table string) *InsertBuilder
	InsertBySql(query string, value ...interface{}) *InsertBuilder

	Update(table string) *UpdateBuilder
	UpdateBySql(query string, value ...interface{}) *UpdateBuilder

	DeleteFrom(table string) *DeleteBuilder
	DeleteBySql(query string, value ...interface{}) *DeleteBuilder
}

type runner interface {
	GetTimeout() time.Duration
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
}

func queryRows(ctx context.Context, runner runner, log EventReceiver, builder Builder, d Dialect) (string, *sql.Rows, error) {
	// discard the timeout set in the runner, the context should not be canceled
	// implicitly here but explicitly by the caller since the returned *sql.Rows
	// may still listening to the context
	i := interpolator{
		Buffer:       NewBuffer(),
		Dialect:      d,
		IgnoreBinary: true,
	}
	err := i.encodePlaceholder(builder, true)
	query, value := i.String(), i.Value()
	if err != nil {
		return query, nil, log.EventErrKv("dbr.select.interpolate", err, kvs{
			"sql":  query,
			"args": fmt.Sprint(value),
		})
	}

	startTime := time.Now()
	defer func() {
		log.TimingKv("dbr.select", time.Since(startTime).Nanoseconds(), kvs{
			"sql": query,
		})
	}()

	traceImpl, hasTracingImpl := log.(TracingEventReceiver)
	if hasTracingImpl {
		ctx = traceImpl.SpanStart(ctx, "dbr.select", query)
		defer traceImpl.SpanFinish(ctx)
	}

	rows, err := runner.QueryContext(ctx, query, value...)
	if err != nil {
		if hasTracingImpl {
			traceImpl.SpanError(ctx, err)
		}
		return query, nil, log.EventErrKv("dbr.select.load.query", err, kvs{
			"sql": query,
		})
	}

	return query, rows, nil
}

func query(ctx context.Context, runner runner, log EventReceiver, builder Builder, d Dialect, dest interface{}) (int, error) {
	timeout := runner.GetTimeout()
	if timeout > 0 {
		var cancel func()
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}

	query, rows, err := queryRows(ctx, runner, log, builder, d)
	if err != nil {
		return 0, err
	}
	count, err := Load(rows, dest)
	if err != nil {
		return 0, log.EventErrKv("dbr.select.load.scan", err, kvs{
			"sql": query,
		})
	}
	return count, nil
}

func queryForMap(ctx context.Context, runner runner, log EventReceiver, builder Builder, d Dialect, dest *[]map[string]interface{}) (int, error) {
	timeout := runner.GetTimeout()
	if timeout > 0 {
		var cancel func()
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}

	query, rows, err := queryRows(ctx, runner, log, builder, d)
	if err != nil {
		return 0, fmt.Errorf("error: %s, query: %s", err, query)
	}
	count, err := MapScan(rows, dest)
	if err != nil {
		err = log.EventErrKv("dbr.select.load.scan", err, kvs{
			"sql": query,
		})

		return 0, fmt.Errorf("error: %s, query: %s", err, query)
	}
	return count, nil
}
