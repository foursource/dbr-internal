package dbr

type Event struct {
	Data      map[string]interface{} `json:"data"`
	Query     map[string]interface{} `json:"query"`
	Schema    string                 `json:"schema"`
	Table     string                 `json:"table"`
	Operation string                 `json:"operation"`
}

// SyncHelper is made to retrieve data from builder
type SyncHelper interface {
	EventQuery()
	EventQueryError(err error)
}
