package dbr

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"github.com/google/uuid"
	"reflect"
	"strings"
)

// InsertStmt builds `INSERT INTO ...`.
type InsertStmt struct {
	runner
	EventReceiver
	SyncEventReceiver
	Dialect

	raw

	Table        string
	Column       []string
	Value        [][]interface{}
	Ignored      bool
	ReturnColumn []string
	RecordID     *int64
	Pid          uuid.UUID
	Payload      []map[string]interface{}
	Ctx          context.Context
	comments     Comments
}

type InsertBuilder = InsertStmt

// Get Pid from transaction if started by one
func (b *InsertStmt) GetPid() uuid.UUID {
	return b.Pid
}

// Get Table Name
func (b *InsertStmt) GetTable() string {
	arrayStrings := strings.Split(b.Table, ".")
	return arrayStrings[1]
}

// Get Schema Name
func (b *InsertStmt) GetSchema() string {
	arrayStrings := strings.Split(b.Table, ".")
	return arrayStrings[0]
}

func (b *InsertStmt) EventQuery() {

	for _, p := range b.Payload {
		b.SyncEventReceiver.EventQuery(
			b.Pid,
			Event{
				Data:      p,
				Table:     b.GetTable(),
				Schema:    b.GetSchema(),
				Operation: "INSERT",
			},
		)
	}
}

func (b *InsertStmt) EventQueryError(err error) {

	if b.Table == "" {
		return
	}

	b.SyncEventReceiver.EventQueryError(
		b.Pid,
		Event{
			Table:     b.GetTable(),
			Schema:    b.GetSchema(),
			Operation: "INSERT",
		},
		err,
	)

}

func (b *InsertStmt) Build(d Dialect, buf Buffer) error {
	if b.raw.Query != "" {
		return b.raw.Build(d, buf)
	}

	if b.Table == "" {
		return ErrTableNotSpecified
	}

	if len(b.Column) == 0 {
		return ErrColumnNotSpecified
	}

	err := b.comments.Build(d, buf)
	if err != nil {
		return err
	}

	if b.Ignored {
		buf.WriteString("INSERT IGNORE INTO ")
	} else {
		buf.WriteString("INSERT INTO ")
	}
	buf.WriteString(d.QuoteIdent(b.Table))

	var placeholderBuf strings.Builder
	placeholderBuf.WriteString("(")
	buf.WriteString(" (")
	for i, col := range b.Column {
		if i > 0 {
			buf.WriteString(",")
			placeholderBuf.WriteString(",")
		}
		buf.WriteString(d.QuoteIdent(col))
		placeholderBuf.WriteString(placeholder)
	}
	buf.WriteString(") VALUES ")
	placeholderBuf.WriteString(")")
	placeholderStr := placeholderBuf.String()

	for i, tuple := range b.Value {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(placeholderStr)

		buf.WriteValue(tuple...)
	}

	buf.WriteString(" RETURNING *")

	return nil
}

// InsertInto creates an InsertStmt.
func InsertInto(table string) *InsertStmt {
	return &InsertStmt{
		Table: table,
	}
}

// InsertInto creates an InsertStmt.
func (sess *Session) InsertInto(table string) *InsertStmt {
	b := InsertInto(table)
	b.runner = sess.Master
	b.EventReceiver = sess.EventReceiver
	b.SyncEventReceiver = sess.SyncEventReceiver
	b.Dialect = sess.Master.Dialect
	b.Payload = []map[string]interface{}{}
	return b
}

// InsertInto creates an InsertStmt.
func (tx *Tx) InsertInto(table string) *InsertStmt {
	b := InsertInto(table)
	b.runner = tx
	b.EventReceiver = tx.EventReceiver
	b.SyncEventReceiver = tx.SyncEventReceiver
	b.Dialect = tx.Dialect
	b.Payload = []map[string]interface{}{}
	b.Pid = tx.Pid
	return b
}

// InsertBySql creates an InsertStmt from raw query.
func InsertBySql(query string, value ...interface{}) *InsertStmt {
	return &InsertStmt{
		raw: raw{
			Query: query,
			Value: value,
		},
	}
}

// InsertBySql creates an InsertStmt from raw query.
func (sess *Session) InsertBySql(query string, value ...interface{}) *InsertStmt {
	b := InsertBySql(query, value...)
	b.runner = sess.Master
	b.EventReceiver = sess.EventReceiver
	b.SyncEventReceiver = sess.SyncEventReceiver
	b.Dialect = sess.Master.Dialect
	b.Payload = []map[string]interface{}{}
	return b
}

// InsertBySql creates an InsertStmt from raw query.
func (tx *Tx) InsertBySql(query string, value ...interface{}) *InsertStmt {
	b := InsertBySql(query, value...)
	b.runner = tx
	b.EventReceiver = tx.EventReceiver
	b.SyncEventReceiver = tx.SyncEventReceiver
	b.Dialect = tx.Dialect
	b.Payload = []map[string]interface{}{}
	b.Pid = tx.Pid
	return b
}

func (b *InsertStmt) Columns(column ...string) *InsertStmt {
	b.Column = column
	return b
}

// Comment adds a comment to prepended. All multi-line sql comment characters are stripped
func (b *InsertStmt) Comment(comment string) *InsertStmt {
	b.comments = b.comments.Append(comment)
	return b
}

// Ignore any insertion errors
func (b *InsertStmt) Ignore() *InsertStmt {
	b.Ignored = true
	return b
}

// Values adds a tuple to be inserted.
// The order of the tuple should match Columns.
func (b *InsertStmt) Values(value ...interface{}) *InsertStmt {
	b.Value = append(b.Value, value)
	return b
}

// Record adds a tuple for columns from a struct.
//
// If there is a field called "Id" or "ID" in the struct,
// it will be set to LastInsertId.
func (b *InsertStmt) Record(structValue interface{}) *InsertStmt {
	v := reflect.Indirect(reflect.ValueOf(structValue))

	if v.Kind() == reflect.Struct {
		found := make([]interface{}, len(b.Column)+1)
		// ID is recommended by golint here
		s := newTagStore()
		s.findValueByName(v, append(b.Column, "id"), found, false)

		value := found[:len(found)-1]
		for i, v := range value {
			if v != nil {
				value[i] = v.(reflect.Value).Interface()
			}
		}

		if v.CanSet() {
			switch idField := found[len(found)-1].(type) {
			case reflect.Value:
				if idField.Kind() == reflect.Int64 {
					b.RecordID = idField.Addr().Interface().(*int64)
				}
			}
		}
		b.Values(value...)
	}
	return b
}

// Returning specifies the returning columns for postgres.
func (b *InsertStmt) Returning(column ...string) *InsertStmt {
	b.ReturnColumn = column
	return b
}

// Pair adds (column, value) to be inserted.
// It is an error to mix Pair with Values and Record.
func (b *InsertStmt) Pair(column string, value interface{}) *InsertStmt {
	b.Column = append(b.Column, column)
	switch len(b.Value) {
	case 0:
		b.Values(value)
	case 1:
		b.Value[0] = append(b.Value[0], value)
	default:
		panic("pair only allows one record to insert")
	}
	return b
}

func (b *InsertStmt) AddContext(ctx context.Context) *InsertStmt {
	b.Ctx = ctx
	return b
}

func (b *InsertStmt) Exec() (sql.Result, error) {
	count, err := b.LoadContext(b.Ctx, &b.Payload)
	return driver.RowsAffected(count), err
}

func (b *InsertStmt) ExecContext(ctx context.Context) (sql.Result, error) {
	count, err := b.LoadContext(ctx, &b.Payload)
	return driver.RowsAffected(count), err
}

func (b *InsertStmt) LoadContext(ctx context.Context, value *[]map[string]interface{}) (int, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	count, err := queryForMap(ctx, b.runner, b.EventReceiver, b, b.Dialect, value)
	if err != nil {
		b.EventQueryError(err)
	} else {
		b.EventQuery()
	}
	return count, err
}

func (b *InsertStmt) Load(value *int) error {
	_, err := b.LoadContext(b.Ctx, &b.Payload)
	if err != nil {
		return err
	}
	if len(b.ReturnColumn) > 0 {
		*value = int(b.Payload[0][b.ReturnColumn[0]].(int64))
	}
	return nil
}
